package com.hackaton.hackupc;

import android.location.Location;

import com.google.android.gms.common.api.Status;

/**
 * Created by eloi on 3/4/17.
 */

public interface LocationUpdaterInterface {
    void locationUpdated(Location location);
    void enableLocation(Status status);
}
