package com.hackaton.hackupc;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Notification extends AppCompatActivity {

    TextView gender, age, distance;
    Button accept, dennie;

    Bundle b;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        b = getIntent().getExtras();

        String NAME = b.getString("NAME");
        String AGE = b.getString("AGE");
        String SEX = b.getString("SEX");
        final String LAT = b.getString("lat");
        final String LON = b.getString("lon");
        String DIST = b.getString("dist");

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle(NAME);
        setSupportActionBar(toolbar);

        gender = (TextView)findViewById(R.id.gender);
        gender.setText(SEX);

        age = (TextView)findViewById(R.id.age);
        age.setText(AGE);

        distance = (TextView)findViewById(R.id.distance);
        distance.setText(DIST);

        accept = (Button)findViewById(R.id.accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + LAT + "," + LON + "&daddr=" + LAT + "," + LON));
                startActivity(intent);
            }
        });

        dennie = (Button)findViewById(R.id.dennie);
        dennie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
