package com.hackaton.hackupc;

import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.text.Text;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;
import android.content.Context;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import com.google.android.gms.common.api.Status;

public class MainActivity extends AppCompatActivity implements LocationUpdaterInterface {

    
    private static String LOG_TAG = "MainActivity";
    private static final int PERMISSION_REQUEST_CODE = 1;
    static String SERVER_URL = "http://35.166.59.33:8000/";
    public int updateMode = 0; // 0 ==> 5 minute intervals, 1 ==> 5 seconds intervals (location update)

    Location loc;
    String str;
    private SharedPreferences sharedPreferences;

    LinearLayout layout_normal, layout_alarmed;

    Button button;

    TextView txtnormal, txtpressed;

    private SeekBar alertLevel;
    ImageButton panicButton;

    private LocationUpdater locationUpdater;
    private boolean locationUpdaterBound = false;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String firebaseInstanceID;

    boolean b = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        layout_normal = (LinearLayout) findViewById(R.id.activity_main_normal);
        layout_alarmed = (LinearLayout)findViewById(R.id.activity_main_alarmed);
        txtnormal = (TextView) findViewById(R.id.txtnormal);
        txtpressed = (TextView)findViewById(R.id.txtpressed);
        button = (Button)findViewById(R.id.despanic);

        boolean firstOpen = sharedPreferences.getBoolean("firstOpen", true);
        if(firstOpen) {
            Intent startRegister = new Intent(this, Register.class);
            startActivity(startRegister);
        }
        
        alertLevel = (SeekBar) findViewById(R.id.alert_level);
        alertLevel.getProgressDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        alertLevel.getThumb().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
        alertLevel.setProgress(0);
        alertLevel.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.i("progress" , String.valueOf(progress));
                switch (progress) {
                    case 0:
                        panicButton.setImageResource(R.drawable.danger01);
                        layout_normal.setBackground(new ColorDrawable(Color.parseColor("#50E30518")));
                        layout_alarmed.setBackground(new ColorDrawable(Color.parseColor("#50E30518")));
                        txtnormal.setText("This is a low level alarm. This button will" +
                                " send to a few of your contacts an alarm alert. ");

                        break;
                    case 1:
                        panicButton.setImageResource(R.drawable.danger02);
                        layout_normal.setBackground(new ColorDrawable(Color.parseColor("#70E30518")));
                        layout_alarmed.setBackground(new ColorDrawable(Color.parseColor("#70E30518")));
                        txtnormal.setText("This is a medium level alarm. This button will" +
                                " send an alarm to the people near you. ");
                        break;
                    case 2:
                        panicButton.setImageResource(R.drawable.danger03);
                        layout_normal.setBackground(new ColorDrawable(Color.parseColor("#85E30518")));
                        layout_alarmed.setBackground(new ColorDrawable(Color.parseColor("#85E30518")));
                        txtnormal.setText("This is a high level alarm. This button will" +
                                " send an alarm to the police.");
                        break;

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        panicButton = (ImageButton) findViewById(R.id.panic_button);

        View.OnClickListener click = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(b) {
                    updateMode = 1;
                    disconnectFromLocationService();
                    startLocation();
                    b = false;
                    layout_alarmed.setVisibility(View.VISIBLE);
                    layout_normal.setVisibility(View.GONE);

                } else {
                    updateMode = 0;
                    startLocation();
                    b = true;
                    layout_alarmed.setVisibility(View.GONE);
                    layout_normal.setVisibility(View.VISIBLE);
                    txtpressed.setText("There are " + str + " people near you to help you.");
                }
            }
        };

        panicButton.setOnClickListener(click);
        button.setOnClickListener(click);

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
            }
        };

        mAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.i("BAAEEEE", "signInAnonymously:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });


        firebaseInstanceID = FirebaseInstanceId.getInstance().getToken();

        startLocation();

    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    public void startLocation() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M &&
                ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_CODE);

        } else {
            Intent intent = new Intent(this, LocationUpdater.class);
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    private String getURLParameters(double lat, double lon) {
        String alert = (updateMode == 1) ? Integer.toString(alertLevel.getProgress()) : "0";
        String name = sharedPreferences.getString("NAME", "NA");
        String sex = sharedPreferences.getString("SEX", "NA");
        String age = Integer.toString(sharedPreferences.getInt("AGE", 0));

        //IP:8000/data?id_data=&name=&gender&age=&level=&lat=&lon=
        if (firebaseInstanceID != null)
            return "data?id_data="+firebaseInstanceID+"&name="+name+"&gender="+sex+"&age="+age+
                            "&level="+alert+"&lat="+Double.toString(lat)+"&long="+Double.toString(lon);
        return null;
    }

    private void sendData(Location location) {
        String params = getURLParameters(location.getLatitude(), location.getLongitude());
        loc = location;
        if (params == null) {
            Log.e(LOG_TAG, "OUPS, firebaseInstanceID may not be initialised!");
            return;
        }
        String url = SERVER_URL + params;
        RequestQueue queue = Volley.newRequestQueue(this);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(updateMode==1)
                            str = response;
                        Log.i(LOG_TAG, response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(LOG_TAG, error.getMessage());
            }
        });
        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(this, LocationUpdater.class);
                    bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
                } else {
                    Log.e(LOG_TAG, "next time allow the app to use location!");
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 199:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(LOG_TAG, "user didn't enable location!");
                        break;
                    default:
                        break;
                }
                break;
        }
    }

    /* LOCATION */
    private void disconnectFromLocationService() {
        if (locationUpdaterBound) {
            locationUpdater.setCallbacks(null); // unregister
            unbindService(serviceConnection);
            locationUpdaterBound = false;
        }
    }
    
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get MyService instance
            LocationUpdater.LocationBinder binder = (LocationUpdater.LocationBinder) service;
            locationUpdater = binder.getService();
            locationUpdater.setCallbacks(MainActivity.this); // register
            locationUpdater.setUpdaterMode(updateMode);
            System.out.println(locationUpdater.getInterval());
            locationUpdaterBound = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            locationUpdaterBound = false;
        }
    };
    
    /* Defined by ServiceCallbacks interface */
    @Override
    public void locationUpdated(Location location) {
        sendData(location);
        if (updateMode == 1) {
            disconnectFromLocationService();
            updateMode = 0;
        }
    }
    
    @Override
    public void enableLocation(Status status) {
        try {
            // and check the result in onActivityResult().
            status.startResolutionForResult(
                    MainActivity.this,
                    199);
        } catch (IntentSender.SendIntentException e) {
            // Ignore the error.
        }
        disconnectFromLocationService();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        disconnectFromLocationService();
    }


}