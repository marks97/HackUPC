package com.hackaton.hackupc;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Calendar;

public class Register extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    CoordinatorLayout layout;

    ImageButton maleButton, femaleButton;
    ImageView gender;
    Button in_age;
    EditText in_name, in_surname;

    String NAME = "", SEX = "";
    int AGE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        /*Timer timer = new Timer();
        MyTimer mt = new MyTimer();
        timer.schedule(mt, 1000, 1000);*/

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        layout = (CoordinatorLayout) findViewById(R.id.register_layout);
        gender = (ImageView)findViewById(R.id.gender);
        maleButton = (ImageButton)findViewById(R.id.maleButton);
        femaleButton = (ImageButton)findViewById(R.id.femaleButton);
        in_name = (EditText)findViewById(R.id.name);
        in_surname = (EditText)findViewById(R.id.surname);
        in_age = (Button)findViewById(R.id.age);

        maleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SEX = "MALE";
                gender.setImageResource(R.drawable.gender_left);
                if(isInfoReady())
                    enterReveal();

            }
        });

        femaleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SEX = "FEMALE";
                gender.setImageResource(R.drawable.gender_rigth);
                if(isInfoReady())
                    enterReveal();
            }
        });

        in_age.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(Register.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                Calendar now = Calendar.getInstance();

                                in_age.setText(dayOfMonth + "   " + (monthOfYear + 1) + "   " + year);
                                AGE = now.get(Calendar.YEAR) - year;
                                if(isInfoReady())
                                    enterReveal();

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        in_name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isInfoReady())
                    enterReveal();
                else
                    exitReveal();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        in_surname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(isInfoReady())
                    enterReveal();
                else
                    exitReveal();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 NAME = in_name.getText().toString();
                    sharedPreferences.edit().putString("SEX", SEX).apply();
                    sharedPreferences.edit().putString("NAME", NAME).apply();
                    sharedPreferences.edit().putInt("AGE", AGE).apply();
                    sharedPreferences.edit().putBoolean("firstOpen", false).commit();
                    finish();
            }
        });
    }

    public void enterReveal() {
        final View myView = findViewById(R.id.fab);
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight()) / 2;
        Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    public void exitReveal() {
        final View myView = findViewById(R.id.fab);
        int cx = myView.getMeasuredWidth() / 2;
        int cy = myView.getMeasuredHeight() / 2;
        int initialRadius = myView.getWidth() / 2;
        Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.INVISIBLE);
            }
        });
        anim.start();
    }

    public boolean isInfoReady() {
        if(!in_name.getText().toString().isEmpty()  && !in_surname.getText().toString().isEmpty()
                && AGE>11 && AGE < 100 && !SEX.isEmpty())
            return true;
        else
            return false;
    }
}
