package com.hackaton.hackupc;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;


public class LocationUpdater extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    static int PANIC_UPDATE_INTERVAL = 5000;        // 5.0 sec
    static int PANIC_FASTEST_INTERVAL = 2000;       // 2.0 sec
    static int UPDATER_UPDATE_INTERVAL = 300000;    // 5.0 min
    static int UPDATER_FASTEST_INTERVAL = 150000;   // 2.5 min
    static int DISPLACEMENT = 70;                   // 70 meters
    int UPDATE_INTERVAL;
    int FASTEST_INTERVAL;
    private GoogleApiClient mGoogleApiClient;
    private final IBinder binder = new LocationBinder();
    private LocationUpdaterInterface locationInterface;
    PendingResult<LocationSettingsResult> result;


    @Override
    public void onCreate() {
        super.onCreate();
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
        mGoogleApiClient.connect();
    }
    @Override
    public void onConnected(Bundle bundle) {
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null && mLastLocation.getAccuracy() <= DISPLACEMENT) {
            locationInterface.locationUpdated(mLastLocation);
        } else {
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(UPDATE_INTERVAL);
            mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
            mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            if (locationInterface != null) {
                                locationInterface.enableLocation(status);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            });
            LocationServices.FusedLocationApi
                    .requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }
    @Override
    public void onConnectionSuspended(int i) {
    }
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }
    @Override
    public void onLocationChanged(Location location) {
        if (location.getAccuracy() <= DISPLACEMENT) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            locationInterface.locationUpdated(location);
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        mGoogleApiClient.connect();
        return START_STICKY;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /* BINDER */
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
    public void setCallbacks(LocationUpdaterInterface locInterface) {
        locationInterface = locInterface;
    }
    public int getInterval() {
        return UPDATE_INTERVAL;
    }
    public void setUpdaterMode(int i) {
        /*
         * If i == 0 => we want 5 minutes intervals
         * If i == 1 => we want 5 seconds intervals (panic button)
         */
        if (i == 0) {
            UPDATE_INTERVAL = UPDATER_UPDATE_INTERVAL;
            FASTEST_INTERVAL = UPDATER_FASTEST_INTERVAL;
        } else {
            UPDATE_INTERVAL = PANIC_UPDATE_INTERVAL;
            FASTEST_INTERVAL = PANIC_FASTEST_INTERVAL;
        }
    }
    public class LocationBinder extends Binder {
        public LocationUpdater getService() {
            return LocationUpdater.this;
        }
    }
}